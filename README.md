# Akka-iot

This is a system written using the akka framework that manages an iot system for home automation. For more info about the design of the system, [read the design document](https://gitlab.com/progetti-middleware-2020-21/project-3/poc/-/tree/master/docs/P3_Design_Document.pdf)

## Overview

The system is clustered, and it is composed of three main actor systems, that may be replicated for availability or for physical needs. The systems can be run in different containers using `docker-compose`, which runs three replicas of a supervisor each in a different container, and three device systems in a single container. The `docker-compose` file also defines a container for the control panel, which is not run with `docker-compose up` in order to give the possibility to be run separately and get access to the command line. 

## Project build

The `docker-compose` file automatically calls gradle (used in this project for build automation tool and dependency manager) to build the application _.jar_ file from the source code

To run the systems from the source folder, type the commands
```sh
docker-compose build
docker-compose up
```

To run and access the control panel afterwards, still from the source folder type
```sh
docker-compose run poc3-controller
```
which will give the access to the control panel interactive menu in command line