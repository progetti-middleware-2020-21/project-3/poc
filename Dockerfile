FROM openjdk:11 as builder
COPY . .
RUN ./gradlew build && ./gradlew jar

FROM openjdk:11

RUN mkdir -p /app/config

WORKDIR /app
COPY --from=builder build/libs/poc-1.0-all.jar /app/poc-1.0-all.jar

RUN chown -R -c 1000:1000 /app
USER 1000

ENV POC3_PARAM = ""

ENTRYPOINT java -jar /app/poc-1.0-all.jar "$POC3_PARAM"
