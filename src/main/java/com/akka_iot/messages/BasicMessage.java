package com.akka_iot.messages;

/**
 * This class is used to generate generic messages
 * that should be intercepted by the HttpServer and translated consenquentially.
 *
 * Some basic implementations are in BasicMessage, used as explained before.
 */
public class BasicMessage extends IoTSerializable {

    public final String msg;

    public BasicMessage(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}

