package com.akka_iot.messages;

public class Messages {
    public static class Success extends BasicMessage {
        public Success(String msg) {
            super(msg);
        }
    }

    public static class NotFound extends BasicMessage {
        public NotFound(String msg) {
            super(msg);
        }
    }

    public static class Error extends BasicMessage {
        public Error(String msg) {
            super(msg);
        }
    }
}
