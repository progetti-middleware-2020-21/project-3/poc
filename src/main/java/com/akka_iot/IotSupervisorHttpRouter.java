package com.akka_iot;

import akka.actor.ActorRef;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.Route;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.akka_iot.managers.DeviceManager;
import com.akka_iot.managers.RoomManager;
import com.akka_iot.messages.Messages;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.http.javadsl.server.Directives.*;
import static akka.http.javadsl.unmarshalling.StringUnmarshallers.UUID;

/**
 * This class contains the definition of the akka-http routes,
 * The HTTP server is instantiated in IoTSupervisor, the route have been separated
 * because of the size of the routing rules
 */
public class IotSupervisorHttpRouter {

    public static final int DEFAULT_AKKA_ASK_TIMEOUT = 3;
    private final IoTSupervisor supervisor;


    public IotSupervisorHttpRouter(IoTSupervisor supervisor) {
        this.supervisor = supervisor;
    }

    public static Timeout timeoutFactory(){
        return new Timeout(Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
    }

    public static Route roomRoutes(IoTSupervisor supervisor){
        return pathPrefix(
            "room",
            () -> concat(
                path(UUID, id -> concat(
                    delete(() -> {
                        Future<Object> futureDeletedRoom = Patterns.ask(supervisor.getMasterSupervisor(), new RoomManager.DeleteRoom(id), timeoutFactory());
                        Object deletedRoom;
                        try {
                            deletedRoom = Await.result(futureDeletedRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
                            if(deletedRoom instanceof Messages.NotFound){
                                return complete(StatusCodes.NOT_FOUND, deletedRoom, Jackson.marshaller());
                            }
                            return complete(StatusCodes.OK, deletedRoom, Jackson.marshaller());
                        } catch (Exception e) {
                            return complete(StatusCodes.INTERNAL_SERVER_ERROR, new Messages.Error("Internal server error"), Jackson.marshaller());
                        }
                    }),
                    get(() -> {
                        Future<Object> futureRoom = Patterns.ask(supervisor.getMasterSupervisor(), new RoomManager.GetRoom(id), timeoutFactory());
                        Object room;
                        try {
                            room = Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
                            if(room instanceof Messages.NotFound){
                                return complete(StatusCodes.NOT_FOUND, room, Jackson.marshaller());
                            }
                            return complete(StatusCodes.OK, room, Jackson.marshaller());
                        } catch (Exception e) {
                            return complete(StatusCodes.INTERNAL_SERVER_ERROR, new Messages.Error("Internal server error"), Jackson.marshaller());
                        }
                    })
                )),
                pathEndOrSingleSlash( () -> concat(
                    get(() -> {
                        Future<Object> ask = Patterns.ask(supervisor.getMasterSupervisor(), new RoomManager.ListRooms(), timeoutFactory());
                        return completeOKWithFuture(ask, Jackson.marshaller());
                    }),
                    put(() ->
                        entity(Jackson.unmarshaller(RoomManager.AddRoom.class), addRoom -> {
                            Future<Object> ask = Patterns.ask(supervisor.getMasterSupervisor(), addRoom, timeoutFactory());
                            return completeOKWithFuture(ask, Jackson.marshaller());
                        })
                    )
                ))
            )
        );
    }

    public static Route deviceRoutes(IoTSupervisor supervisor){
        return pathPrefix(
            "device",
            () -> concat(
                path(UUID, id -> concat(
                        get(() -> {
                            Future<Object> futureDevice = Patterns.ask(supervisor.getMasterSupervisor(), new DeviceManager.GetDevice(id), timeoutFactory());
                            Object device;
                            try {
                                device = Await.result(futureDevice, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
                                if(device instanceof Messages.NotFound){
                                    return complete(StatusCodes.NOT_FOUND, futureDevice, Jackson.marshaller());
                                }
                                return complete(StatusCodes.OK, device, Jackson.marshaller());
                            } catch (Exception e) {
                                return complete(StatusCodes.INTERNAL_SERVER_ERROR, new Messages.Error("Internal server error"), Jackson.marshaller());
                            }
                        }),
                        delete(() -> {
                            Future<Object> futureDeletedDevice = Patterns.ask(supervisor.getMasterSupervisor(), new DeviceManager.DeleteDevice(id), timeoutFactory());
                            Object deletedDevice;
                            try {
                                deletedDevice = Await.result(futureDeletedDevice, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
                                if(deletedDevice instanceof Messages.NotFound){
                                    return complete(StatusCodes.NOT_FOUND, deletedDevice, Jackson.marshaller());
                                }
                                return complete(StatusCodes.OK, deletedDevice, Jackson.marshaller());
                            } catch (Exception e) {
                                return complete(StatusCodes.INTERNAL_SERVER_ERROR, new Messages.Error("Internal server error"), Jackson.marshaller());
                            }
                        }),
                        patch(() ->
                            entity(Jackson.unmarshaller(DeviceManager.EditDevice.class), editDevice -> {
                                editDevice.setId(id);
                                Future<Object> ask = Patterns.ask(supervisor.getMasterSupervisor(), editDevice, timeoutFactory());
                                return completeOKWithFuture(ask, Jackson.marshaller());
                            })
                        )
                )),
                pathEndOrSingleSlash( () -> concat(
                    get(() -> {
                        Future<Object> ask = Patterns.ask(supervisor.getMasterSupervisor(), new DeviceManager.ListDevices(), timeoutFactory());
                        return completeOKWithFuture(ask, Jackson.marshaller());
                    }),
                    put(() ->
                        entity(Jackson.unmarshaller(DeviceManager.AddDevice.class), addDevice -> {
                            Future<Object> ask = Patterns.ask(supervisor.getMasterSupervisor(), addDevice, timeoutFactory());
                            return completeOKWithFuture(ask, Jackson.marshaller());
                        })
                    )
                ))
            )
        );
    }

    public Route getRouter() {
        return concat(
            roomRoutes(supervisor),
            deviceRoutes(supervisor)
        );
    }
}
