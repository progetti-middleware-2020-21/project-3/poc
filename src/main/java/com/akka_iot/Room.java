package com.akka_iot;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.ActorRef;
import com.akka_iot.devices.AirConditioner;
import com.akka_iot.devices.Device;
import com.akka_iot.devices.Heat;
import com.akka_iot.devices.TemperatureSensor;
import com.akka_iot.managers.RoomManager;
import com.akka_iot.messages.IoTSerializable;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.akka_iot.devices.DeviceActorFactory.*;

/**
 * The actor for a single room. Manages the temperature reading the sensors, starting the AC or the furnace and
 * opening vents.
 */
public class Room extends AbstractActor {

    private final String name;
    private final int floor;
    private final int size;
    private final UUID                  id;
    private final Map<String, ActorRef> sensors = new HashMap<>();
    private final Map<String, ActorRef> ac = new HashMap<>();
    private final Map<String, ActorRef> heat = new HashMap<>();
    private        float  temperature;
    private        float  targetTemperature = 20f;
    private static Logger log = LoggerFactory.getLogger(IoTMain.class);


    public Room(String name, int floor, int size, UUID id, float temperature, float targetTemperature) {
        this.floor = floor;
        this.name = name;
        this.size = size;
        this.id = id;
        this.temperature = temperature;
        this.targetTemperature = targetTemperature;
    }

    public int getFloor() {
        return floor;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public UUID getId() { return id; }

    public static Props props(String name, int floor, int size, UUID id, float temperature) {
        return Props.create(Room.class, name, floor, size, id, temperature, 20f);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Device.DeviceData.class, this::addDevice)
                .match(IoTSupervisor.NewSupervisor.class, this::newSupervisor)
                .match(RoomManager.RoomIntel.class, this::newRoomIntel)
                .match(RoomInfo.class, this::roomInfo)
                .match(TemperatureSensor.RespondTemperature.class, this::newTemperature)
                .match(TemperatureRequest.class, this::temperatureRequest)
                .match(TargetTemperatureRequest.class, this::targetTemperatureRequest)
                .match(SetTargetTemperature.class, this::setTargetTemperature)
                .match(DevicesRequest.class, this::devicesRequest)
                .build();
    }
    
    private void setTargetTemperature(SetTargetTemperature setTargetTemperature) {
        targetTemperature = setTargetTemperature.targetTemperature;
    }
    
    private void devicesRequest(DevicesRequest request) {
        List<String> lines = new ArrayList<>();
        lines.addAll(sensors.keySet().stream().map(s -> "SENSOR \t" + s).collect(Collectors.toList()));
        lines.addAll(heat.keySet().stream().map(s ->    "HEAT   \t" + s).collect(Collectors.toList()));
        lines.addAll(ac.keySet().stream().map(s ->      "AC     \t" + s).collect(Collectors.toList()));
        getSender().tell(new DevicesResponse(lines.toArray(new String[0])), getSelf());
    }
    
    private void targetTemperatureRequest(TargetTemperatureRequest request) {
        getSender().tell(new TargetTemperatureResponse(targetTemperature), getSelf());
    }
    
    private void temperatureRequest(TemperatureRequest request) {
        getSender().tell(new TemperatureResponse(temperature), getSelf());
    }
    
    private void newTemperature(TemperatureSensor.RespondTemperature temperature) {
        float oldTemp = Room.this.temperature;
        if (Float.isNaN(Room.this.temperature)) {
            Room.this.temperature = temperature.value;
        } else {
            Room.this.temperature = oldTemp + 0.1f * (temperature.value - oldTemp);
        }
        log.info("The current temperature is {}. (Previous: {}, Sensor: {})", Room.this.temperature, oldTemp, temperature.value);
    }
    
    private void newRoomIntel(RoomManager.RoomIntel roomIntel) {
        sensors.clear();
        targetTemperature = roomIntel.targetTemperature;
        for (String address : roomIntel.sensors) {
            sensors.put(address, getContext().actorSelection(address).anchor());
        }
    }
    
    private void newSupervisor(IoTSupervisor.NewSupervisor newSupervisor) {
        getContext().actorSelection(newSupervisor.address).tell(new RoomManager.RoomIntel(name, floor, size, id, sensors.keySet().toArray(new String[0]), temperature, targetTemperature),
                getSender());
    }
    
    private void addDevice(Device.DeviceData deviceData) {
        if ((deviceData.room != null) && (deviceData.room.id.equals(id))) {
            if (deviceData.deviceType.equals(TEMPERATURE_SENSOR) && !sensors.containsKey(deviceData.address)) {
                addToMap(deviceData, sensors);
            }
            if (deviceData.deviceType.equals(AC) && !sensors.containsKey(deviceData.address)) {
                addToMap(deviceData, ac);
            }
            if (deviceData.deviceType.equals(HEATING) && !sensors.containsKey(deviceData.address)) {
                addToMap(deviceData, heat);
            }
        } else {
            if (deviceData.deviceType.equals(TEMPERATURE_SENSOR)) {
                sensors.remove(deviceData.address);
            }
            if (deviceData.deviceType.equals(AC)) {
                ac.remove(deviceData.address);
            }
            if (deviceData.deviceType.equals(HEATING)) {
                heat.remove(deviceData.address);
            }
        }
    }
    
    private void addToMap(Device.DeviceData deviceData, Map<String, ActorRef> map) {
        ActorSelection actorSelection = getContext().actorSelection(deviceData.address);
        ActorRef ref = actorSelection.anchor();
        map.put(deviceData.address,
                ref);
        ref.tell(new IoTSupervisor.Ping(), getSelf());
    }
    
    @Override
    public void preStart() throws Exception {
        super.preStart();
        getContext().getSystem().getScheduler().scheduleWithFixedDelay(
                Duration.ofSeconds(1),
                Duration.ofSeconds(1), () -> {
                    if (!Float.isNaN(temperature) && !Float.isNaN(targetTemperature)) {
                        if (targetTemperature - 1.5 > temperature) {
                            for (String ref : heat.keySet()) {
                                getContext().actorSelection(ref).tell(new Heat.ChangeState(true), getSelf());
                            }
                        }
                        if (targetTemperature < temperature) {
                            for (String ref : heat.keySet()) {
                                getContext().actorSelection(ref).tell(new Heat.ChangeState(false), getSelf());
                            }
                        }
                        if (targetTemperature + 1.5 < temperature) {
                            for (String ref : ac.keySet()) {
                                getContext().actorSelection(ref).tell(new AirConditioner.ChangeState(true), getSelf());
                            }
                        }
                        if (targetTemperature > temperature) {
                            for (String ref : ac.keySet()) {
                                getContext().actorSelection(ref).tell(new AirConditioner.ChangeState(false), getSelf());
                            }
                        }
                    }
                    for (String ref : sensors.keySet()) {
                        getContext().actorSelection(ref).tell(new TemperatureSensor.ReadTemperature(), getSelf());
                    }
                    log.info("Temperature has been requested!");
                },
                getContext().getSystem().getDispatcher());
    }
    
    public void roomInfo(RoomInfo roomInfo){
        sender().tell(new RoomData(name, floor, size, id, temperature, targetTemperature), self());
    }
    
    public static class RoomMessage extends RoomManager.RoomManagerMessage {
        public final UUID id;
    
        public RoomMessage(@JsonProperty("id") UUID id) {
            this.id = id;
        }
    }
    
    public static class TemperatureRequest extends RoomMessage {
    
        public TemperatureRequest(@JsonProperty("id") UUID id) {
            super(id);
        }
    
    }
    
    public static class TargetTemperatureRequest extends RoomMessage {
    
        public TargetTemperatureRequest(@JsonProperty("id") UUID id) {
            super(id);
        }
    
    }
    
    public static class DevicesRequest extends RoomMessage {
    
        public DevicesRequest(@JsonProperty("id") UUID id) {
            super(id);
        }
    
    }
    
    public static class SetTargetTemperature extends RoomMessage {
        public final float targetTemperature;
        
        public SetTargetTemperature(@JsonProperty("id") UUID id, @JsonProperty("targetTemperature") float targetTemperature) {
            super(id);
            this.targetTemperature = targetTemperature;
        }
    }
    
    public static class TemperatureResponse extends IoTSerializable {
        public final float temperature;
    
        public TemperatureResponse(@JsonProperty("temperature") float temperature) {
            this.temperature = temperature;
        }
    
    }
    public static class TargetTemperatureResponse extends IoTSerializable {
        public final float targetTemperature;
    
        public TargetTemperatureResponse(@JsonProperty("targetTemperature") float targetTemperature) {
            this.targetTemperature = targetTemperature;
        }
    }
    public static class DevicesResponse extends IoTSerializable {
        public final String[] devices;
    
        public DevicesResponse(@JsonProperty("devices") String[] devices) {
            this.devices = devices;
        }
    }

    public static class RoomInfo extends IoTSerializable {}

    public static class RoomData extends IoTSerializable {
        public final String name;
        public final int floor;
        public final int size;
        public final UUID id;
        public final float temperature;
        public final float targetTemperature;

        @JsonCreator
        public RoomData(
                @JsonProperty("name") String name,
                @JsonProperty("floor") int floor,
                @JsonProperty("size") int size,
                @JsonProperty("id") UUID id,
                @JsonProperty("temperature") float temperature,
                @JsonProperty("targetTemperature") float targetTemperature
                
        ){
            this.name = name;
            this.floor = floor;
            this.size = size;
            this.id = id;
            this.temperature = temperature;
            this.targetTemperature = targetTemperature;
        }
    }
    
    
}