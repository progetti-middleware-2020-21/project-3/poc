package com.akka_iot;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.cluster.Cluster;
import com.akka_iot.devices.AirConditioner;
import com.akka_iot.devices.Heat;
import com.akka_iot.devices.TemperatureSensor;
import com.akka_iot.managers.ControlManager;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class IoTMain {

    public static final List<String> VALID_ROLES = Arrays.asList(
        "supervisor",
        "control-panel",
        "ac",
        "heat",
        "sensor",
        "devices"
    );
    
    private static Logger log = LoggerFactory.getLogger(IoTMain.class);
    public static final boolean LOG_CONTROL_PANEL = false;
    
    public static void main(String[] args) throws UnknownHostException {
        if (args.length == 0) {
            startup("supervisor", 33221);
        } else if (args.length == 1) {
            if(!VALID_ROLES.contains(args[0])){
                throw new IllegalArgumentException("Expected first arg: " + String.join(", ", VALID_ROLES));
            }
            switch (args[0]) {
                case "supervisor": {
                    startup("supervisor", 33221);
                    break;
                }
                case "control-panel": {
                    startup("control-panel", 33222);
                    break;
                }
                case "devices": {
                    startup("ac", 0);
                    startup("heat", 0);
                    startup("temp-sensor", 0);
                    break;
                }
                default: {
                    startup(args[0], 0);
                }
            }
        } else if (args.length == 2) {
            startup(args[0], Integer.parseInt(args[1]));
        } else {
            throw new IllegalArgumentException("Usage: role port");
        }
    
    
        if (atTheEnd != null) {
            atTheEnd.run();
        }
    }
    
    private static Runnable atTheEnd = null;
    
    private static void startup(String role, int port) throws UnknownHostException {
        List roles;
        if (role.equals("temp-sensor")) {
            roles = Arrays.asList("device", "sensor");
        } else if (role.equals("heat")) {
            roles = Arrays.asList("device", "heat");
        } else if (role.equals("ac")) {
            roles = Arrays.asList("device", "ac");
        } else {
            roles = Collections.singletonList(role);
        }
        Map<String, Object> overrides = new HashMap<>();
        overrides.put("akka.remote.artery.canonical.port", port);
        overrides.put("akka.remote.artery.canonical.hostname", InetAddress.getLocalHost().getHostName());
        overrides.put("akka.cluster.roles", roles);
        if (role.equals("control-panel") && !LOG_CONTROL_PANEL) {
            overrides.put("akka.stdout-loglevel", "OFF");
            overrides.put("akka.loglevel", "OFF");
        }
        
        Config config = ConfigFactory.parseMap(overrides)
                .withFallback(ConfigFactory.load("application"));
        
        ActorSystem system = ActorSystem.create("iot-system", config);
        
        Cluster cluster = Cluster.get(system);
    
        if (cluster.selfMember().hasRole("supervisor")) {
            system.actorOf(IoTSupervisor.props(), "iot-supervisor");
        }
        if (cluster.selfMember().hasRole("control-panel")) {
            ActorRef controlPanel = system.actorOf(ControlManager.props(), "control-panel");
            controlPanel.tell(new ControlManager.MenuInput(""), controlPanel);
            atTheEnd = () -> {
                Scanner in = new Scanner(System.in);
                while (true) {
                    String nextLine = in.nextLine();
                    controlPanel.tell(new ControlManager.MenuInput(nextLine), controlPanel);
                }
            };
        }
        
        if (cluster.selfMember().hasRole("device")) {
            if (cluster.selfMember().hasRole("sensor")) {
                UUID id = UUID.randomUUID();
                system.actorOf(TemperatureSensor.props(id), "device");
            }
            if (cluster.selfMember().hasRole("heat")) {
                UUID id = UUID.randomUUID();
                system.actorOf(Heat.props(id), "device");
            }
            if (cluster.selfMember().hasRole("ac")) {
                UUID id = UUID.randomUUID();
                system.actorOf(AirConditioner.props(id), "device");
            }
        }
        log.info("Current roles: {}, requested roles: {}", cluster.selfMember().getRoles(), roles);
        
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Recived sigint, terminating smart-home system");
            system.terminate();
        }));
        
    }
    
    
}
