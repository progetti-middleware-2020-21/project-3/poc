package com.akka_iot.managers;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;

/**
 * Abstract class representing an actor manager,
 *
 * In the project it has been implemented as DeviceManager (for device management)
 * and RoomManager (for room management)
 */
abstract public class AbstractManager extends AbstractActor  {
    public static final int DEFAULT_AKKA_ASK_TIMEOUT = 1;

    protected final ActorRef supervisor;
    protected final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    public AbstractManager(ActorRef supervisor){
        this.supervisor = supervisor;
    }
}
