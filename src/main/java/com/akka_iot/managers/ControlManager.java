package com.akka_iot.managers;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import com.akka_iot.IoTMain;
import com.akka_iot.Room;
import com.akka_iot.devices.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;

public class ControlManager extends AbstractActor {
    
    private static Logger log = LoggerFactory.getLogger(IoTMain.class);
    
    private String                                currentState = "start";
    private String                                line         = "";
    private Map<String, Function<String, String>> fsm          = new HashMap<>();
    private Function<Object, Object>              waiting      = null;
    private List<Member>                          supervisors  = new ArrayList<>();
    private Member                                supervisor   = null;
    private Device.DeviceData[]                   devices      = null;
    private Device.DeviceData                     device       = null;
    private Room.RoomData[]                       rooms        = null;
    private Room.RoomData                         room         = null;
    private RoomManager.AddRoom                   addRoom      = null;
    private String[]                              roomDevices  = null;
    private float                                 roomTemp     = 0;
    private float                                 roomTarget   = 0;
    
    public static Props props() {
        return Props.create(ControlManager.class);
    }
    
    @Override
    public void preStart() throws Exception {
        super.preStart();
        
        Cluster cluster = Cluster.get(getContext().getSystem());
        cluster.subscribe(
                getSelf(),
                ClusterEvent.initialStateAsEvents(),
                ClusterEvent.MemberEvent.class,
                ClusterEvent.UnreachableMember.class);
        
        fsm.put("start", (line) -> {
            System.out.print("My home Control panel.\n\n\t1 - Monitor rooms\n\t2 - Manage rooms\n\t3 - " +
                    "Manage devices\n\nWhat do you want to do?  ");
            return "menu";
        });
        fsm.put("menu", (line) -> {
            if (supervisor != null) {
                if (line.matches("[0-9]+")) {
                    int chosen = Integer.parseInt(line);
                    if ((chosen > 0) && (chosen <= 3)) {
                        switch (chosen) {
                            case 1: {
                                System.out.print("\nMonitor rooms.\n\n");
                                getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new RoomManager.ListRooms(),
                                        getSelf());
                                waiting = o -> {
                                    RoomManager.RoomList roomList = (RoomManager.RoomList) o;
                                    ControlManager.this.rooms = roomList.rooms;
                                    Room.RoomData[] rooms = roomList.rooms;
                                    for (int i = 0; i < rooms.length; i++) {
                                        Room.RoomData roomData = rooms[i];
                                        System.out.println(String.format(
                                                "\t%d - Name: %s, Floor: %d, Size: %d",
                                                i + 1,
                                                roomData.name,
                                                roomData.floor,
                                                roomData.size
                                        ));
                                    }
                                    System.out.print("\tB - Back\n\nChoose a room:  ");
                                    return null;
                                };
                                return "rooms_monitor";
                            }
                            case 2: {
                                System.out.print("\nManage rooms.\n\n");
                                getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new RoomManager.ListRooms(),
                                        getSelf());
                                waiting = o -> {
                                    RoomManager.RoomList roomList = (RoomManager.RoomList) o;
                                    ControlManager.this.rooms = roomList.rooms;
                                    Room.RoomData[] rooms = roomList.rooms;
                                    for (int i = 0; i < rooms.length; i++) {
                                        Room.RoomData roomData = rooms[i];
                                        System.out.println(String.format(
                                                "\t%d - Name: %s, Id: %s, Floor: %d, Size: %d",
                                                i + 1,
                                                roomData.name,
                                                roomData.id,
                                                roomData.floor,
                                                roomData.size
                                        ));
                                    }
                                    System.out.print("\tA - Add room\n\tB - Back\n\nChoose an option: ");
                                    return null;
                                };
                                return "rooms_manage";
                            }
                            case 3: {
                                System.out.print("\nManage devices.\n\n");
                                getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new DeviceManager.ListDevices(),
                                        getSelf());
                                waiting = o -> {
                                    DeviceManager.DeviceList deviceList = (DeviceManager.DeviceList) o;
                                    ControlManager.this.devices = deviceList.devices;
                                    if (deviceList.devices.length == 0) {
                                        System.out.println("\tThere is no device to see. Try connecting some devices.\n");
                                        currentState = "start";
                                        self().tell(new MenuInput(""), getSelf());
                                    } else {
                                        Device.DeviceData[] devices = deviceList.devices;
                                        for (int i = 0; i < devices.length; i++) {
                                            Device.DeviceData deviceData = devices[i];
                                            System.out.println(String.format("\t%d - Device %s, (%s)",
                                                    i + 1,
                                                    deviceData.id.toString(),
                                                    deviceData.deviceType));
                                        }
                                        System.out.print("\tB - Back\n\nChoose a device to manage: ");
                                    }
                                    return null;
                                };
                                return "devices_manage";
                            }
                        }
                    }
                }
                System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
                fsm.get("start").apply("");
                return "menu";
            } else {
                fsm.get("error").apply("");
                return "error";
            }
        });
        fsm.put("rooms_manage", (line) -> {
            
            if (line.matches("[0-9]+")) {
                int chosen = Integer.parseInt(line);
                if ((chosen > 0) && (chosen <= rooms.length)) {
                    room = rooms[chosen - 1];
                    System.out.print(String.format(
                            "\nRoom info:\n\tName: %s\n\tId: %s\n\tFloor: %d\n\tSize: %d\n\nManage Room\n\n\tD - Delete room\n\tB - Back\n\nChoose an option: ",
                            room.name,
                            room.id,
                            room.floor,
                            room.size
                    ));
                    return "room";
                }
            } else if (line.toUpperCase().equals("B")) {
                System.out.println("");
                fsm.get("start").apply("");
                return "menu";
            } else if (line.toUpperCase().equals("A")) {
                System.out.print("\nProvide the info for the new room. (At any time, insert an empty string to cancel)\n\nNew room name:  ");
                return "add_room_name";
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            fsm.get("menu").apply("2");
            return "rooms_manage";
        });
        fsm.put("rooms_monitor", (line) -> {
            
            if (line.matches("[0-9]+")) {
                int chosen = Integer.parseInt(line);
                if ((chosen > 0) && (chosen <= rooms.length)) {
                    room = rooms[chosen - 1];
                    System.out.print(String.format(
                            "\nRoom info:\n\tName: %s\n\tFloor: %d\n\tSize: %d",
                            room.name,
                            room.floor,
                            room.size
                    ));
                    final int[] arriving = new int[] {0};
                    waiting = new Function<>() {
                        @Override
                        public Object apply(Object o) {
                            if (o instanceof Room.TemperatureResponse) {
                                roomTemp = ((Room.TemperatureResponse) o).temperature;
                            }
                            if (o instanceof Room.TargetTemperatureResponse) {
                                roomTarget = ((Room.TargetTemperatureResponse) o).targetTemperature;
                            }
                            if (o instanceof Room.DevicesResponse) {
                                roomDevices = ((Room.DevicesResponse) o).devices;
                            }
                            if (arriving[0] == 2) {
                                System.out.printf("\n\tCurrent temperature: %.2f ºC\n\tTarget temperature: %.2f ºC\n\tDevices: %s",
                                        roomTemp,
                                        roomTarget,
                                        String.join("", Arrays.stream(roomDevices).map(this::apply).toArray(String[]::new)));
                                System.out.println("\n\nManage room\n\n\t1 - Set target temperature\n\tB - Back\n\nChoose an option: ");
                            } else {
                                waiting = this;
                                arriving[0]++;
                            }
                            return null;
                        }
                        
                        private String apply(String s) {
                            return "\n\t\t" + s;
                        }
                    };
                    ActorSelection iotSupervisor = getContext().actorSelection(supervisor.address() + "/user/iot-supervisor");
                    iotSupervisor.tell(new Room.TemperatureRequest(room.id),
                            getSelf());
                    iotSupervisor.tell(new Room.TargetTemperatureRequest(room.id),
                            getSelf());
                    iotSupervisor.tell(new Room.DevicesRequest(room.id),
                            getSelf());
                    return "room_monitor";
                }
            } else if (line.toUpperCase().equals("B")) {
                System.out.println("");
                fsm.get("start").apply("");
                return "menu";
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            fsm.get("menu").apply("1");
            return "rooms_monitor";
        });
        fsm.put("room_monitor", (line) -> {
            if (line.toUpperCase().equals("1")) {
                System.out.print("\nType the new target temperature (empty to cancel): ");
                return "room_monitor_set_temp";
            } else if (line.toUpperCase().equals("B")) {
                return fsm.get("menu").apply("1");
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            return fsm.get("rooms_monitor").apply((Arrays.asList(rooms).indexOf(room) + 1 )+ "");
        });
        fsm.put("room_monitor_set_temp", (line) -> {
            if (line.isEmpty()) {
                return fsm.get("rooms_monitor").apply((Arrays.asList(rooms).indexOf(room) + 1 ) + "");
            } else if (line.matches("[0-9]+(\\.[0-9]+)?")) {
                getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new Room.SetTargetTemperature(room.id, Float.parseFloat(line)),
                        getSelf());
                return fsm.get("rooms_monitor").apply((Arrays.asList(rooms).indexOf(room) + 1 ) + "");
            }
            System.out.print("\nThe value you put is not valid. Retry or empty to cancel:  ");
            return "room_monitor_set_temp";
        });
        fsm.put("add_room_name", (line) -> {
            if (line.isEmpty()) {
                fsm.get("menu").apply("2");
                return "rooms_manage";
            } else {
                System.out.print("New room floor:  ");
                addRoom = new RoomManager.AddRoom(line, 0, 0);
                return "add_room_floor";
            }
        });
        fsm.put("add_room_floor", (line) -> {
            if (line.isEmpty()) {
                fsm.get("menu").apply("2");
                return "rooms_manage";
            } else {
                if (line.matches("-?[0-9]+")) {
                    int chosen = Integer.parseInt(line);
                    addRoom = new RoomManager.AddRoom(addRoom.name, chosen, 0);
                    System.out.print("New room size:  ");
                    return "add_room_size";
                } else {
                    System.out.print("The floor has to be an integer number.\n\nNew room floor:  ");
                    return "add_room_floor";
                }
            }
        });
        fsm.put("add_room_size", (line) -> {
            if (line.isEmpty()) {
                fsm.get("menu").apply("2");
                return "rooms_manage";
            } else {
                if (line.matches("[0-9]+")) {
                    int chosen = Integer.parseInt(line);
                    addRoom = new RoomManager.AddRoom(addRoom.name, addRoom.floor, chosen);
                    getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(addRoom,
                            getSelf());
                    System.out.println("\n\nA new room has been created!\n\n");
                    fsm.get("menu").apply("2");
                    return "rooms_manage";
                } else {
                    System.out.print("The floor has to be an integer number.\n\nNew room size:  ");
                    return "add_room_size";
                }
            }
        });
        fsm.put("room", (line) -> {
            if (line.toUpperCase().equals("D")) {
                getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new RoomManager.DeleteRoom(room.id),
                        getSelf());
                System.out.println("\nThe room has been deleted!");
                return fsm.get("menu").apply("2");
            } else if (line.toUpperCase().equals("B")) {
                return fsm.get("menu").apply("2");
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            return fsm.get("rooms_manage").apply((Arrays.asList(rooms).indexOf(room) + 1 ) + "");
        });
        fsm.put("devices_manage", (line) -> {
            
            if (line.matches("[0-9]+")) {
                int chosen = Integer.parseInt(line);
                if ((chosen > 0) && (chosen <= devices.length)) {
                    device = devices[chosen - 1];
                    System.out.print(String.format(
                            "\nDevice info:\n\tIdentifier: %s\n\tType: %s\n\tRoom: %s\n\nManage device\n\n\t1 - %s\n\tB - Back\n\nWhat do you want to do?",
                            device.id,
                            device.deviceType,
                            device.room == null ? "No room" : String.format(
                                    "\n\t\tName: %s\n\t\tId: %s\n\t\tFloor: %d\n\t\tSize: %d",
                                    device.room.name,
                                    device.room.id,
                                    device.room.floor,
                                    device.room.size
                            ),
                            device.room == null ? "Join room" : "Remove from room"
                    ));
                    return "device";
                }
            } else if (line.toUpperCase().equals("B")) {
                System.out.println("");
                fsm.get("start").apply("");
                return "menu";
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            fsm.get("menu").apply("3");
            return "devices_manage";
        });
        fsm.put("device", (line) -> {
            if (line.equals("1")) {
                if (device.room == null) {
                    
                    System.out.println("\nJoin room with device\n\n");
                    getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new RoomManager.ListRooms(),
                            getSelf());
                    waiting = o -> {
                        RoomManager.RoomList roomList = (RoomManager.RoomList) o;
                        ControlManager.this.rooms = roomList.rooms;
                        Room.RoomData[] rooms = roomList.rooms;
                        for (int i = 0; i < rooms.length; i++) {
                            Room.RoomData roomData = rooms[i];
                            System.out.println(String.format(
                                    "\t%d - Name: %s, Id: %s, Floor: %d, Size: %d",
                                    i + 1,
                                    roomData.name,
                                    roomData.id,
                                    roomData.floor,
                                    roomData.size
                            ));
                        }
                        System.out.print("\tB - Back\n\nChoose a room to join: ");
                        return null;
                    };
                    return "device_room_join";
                } else {
                    System.out.print("\nRemoved from room!\n\n");
                    getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new DeviceManager.EditDeviceWithID(device.id, device.deviceType,
                                    null),
                            getSelf());
                    return fsm.get("menu").apply("3");
                }
            } else if (line.toUpperCase().equals("B")) {
                fsm.get("menu").apply("3");
                return "devices_manage";
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            fsm.get("devices_manage").apply((Arrays.asList(devices).indexOf(device) + 1) + "");
            return "device";
        });
        fsm.put("device_room_join", (line) -> {
            
            if (line.matches("[0-9]+")) {
                int chosen = Integer.parseInt(line);
                if ((chosen > 0) && (chosen <= rooms.length)) {
                    room = rooms[chosen - 1];
                    getContext().actorSelection(supervisor.address() + "/user/iot-supervisor").tell(new DeviceManager.EditDeviceWithID(device.id, device.deviceType,
                                    room.id),
                            getSelf());
                    System.out.println("Device has joined the room!\n");
                    return fsm.get("menu").apply("3");
                }
            } else if (line.toUpperCase().equals("B")) {
                return fsm.get("menu").apply("3");
            }
            System.out.print("\nThis option is not valid. Please provide a valid one.\n\n");
            fsm.get("menu").apply("2");
            return "device_room)_join";
        });
        fsm.put("error", (line) -> {
            System.out.print("The system is not currently working or in error. Please retry.\n\n");
            self().tell(new MenuInput(""), getSelf());
            return "menu";
        });
    }
    
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MenuInput.class, this::startMenu)
                .match(DeviceManager.DeviceList.class, this::deviceListReceived)
                .match(RoomManager.RoomList.class, this::roomListReceived)
                .match(Room.DevicesResponse.class, this::responseDevices)
                .match(Room.TemperatureResponse.class, this::responseTemperature)
                .match(Room.TargetTemperatureResponse.class, this::responseTargetTemperature)
                .match(ClusterEvent.MemberUp.class, this::memberUp)
                .match(ClusterEvent.MemberDowned.class, this::memberDown)
                .build();
    }
    
    private void startMenu(MenuInput s) {
        if (fsm.containsKey(currentState)) {
            currentState = fsm.get(currentState).apply(s.input);
            if (!fsm.containsKey(currentState)) {
                self().tell(new MenuInput(""), getSelf());
            }
        } else {
            if (!currentState.equals("exit")) {
                System.out.print("State not supported! (" + currentState + ")\n\n");
                currentState = "start";
                self().tell(new MenuInput(""), getSelf());
            }
        }
    }
    
    private void deviceListReceived(DeviceManager.DeviceList deviceList) {
        advanceWaiting(deviceList);
    }
    
    private void roomListReceived(RoomManager.RoomList roomList) {
        if (waiting != null) {
            waiting.apply(roomList);
            waiting = null;
        }
    }
    
    private void responseDevices(Room.DevicesResponse response) {
        this.roomDevices = response.devices;
        advanceWaiting(response);
    }
    
    private void responseTemperature(Room.TemperatureResponse response) {
        this.roomTemp = response.temperature;
        advanceWaiting(response);
    }
    
    private void responseTargetTemperature(Room.TargetTemperatureResponse response) {
        this.roomTarget = response.targetTemperature;
        advanceWaiting(response);
    }
    
    private void memberUp(ClusterEvent.MemberUp mUp) {
        if (IoTMain.LOG_CONTROL_PANEL) {
            log.info("Member up: {}", mUp.member());
        }
        if (mUp.member().hasRole("supervisor")) {
            supervisors.add(mUp.member());
            supervisor = supervisors.stream().reduce((memberA, memberB) -> {
                if (memberA.hashCode() < memberB.hashCode()) {
                    return memberA;
                } else {
                    return memberB;
                }
            }).orElse(null);
        }
    }
    
    private void memberDown(ClusterEvent.MemberDowned mDown) {
        if (IoTMain.LOG_CONTROL_PANEL) {
            log.info("Member downed: {}", mDown.member());
        }
        if (mDown.member().hasRole("supervisor") && (supervisors.contains(mDown.member()))) {
            supervisors.remove(mDown.member());
            supervisor = supervisors.stream().reduce((memberA, memberB) -> {
                if (memberA.hashCode() < memberB.hashCode()) {
                    return memberA;
                } else {
                    return memberB;
                }
            }).orElse(null);
        }
    }
    
    private void advanceWaiting(Object object) {
        if (waiting != null) {
            Function<Object, Object> old = waiting;
            waiting = null;
            old.apply(object);
        }
    }
    
    public static class MenuInput {
        
        public final String input;
        
        public MenuInput(String input) {
            this.input = input;
        }
        
    }
    
}
