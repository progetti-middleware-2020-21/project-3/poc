package com.akka_iot.managers;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.pattern.Patterns;
import com.akka_iot.IoTSupervisor;
import com.akka_iot.devices.Device;
import com.akka_iot.devices.DeviceActorFactory;
import com.akka_iot.messages.IoTSerializable;
import com.akka_iot.messages.Messages;
import com.fasterxml.jackson.annotation.JsonProperty;
import scala.concurrent.Await;
import scala.concurrent.Awaitable;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DeviceManager extends AbstractManager {
    
    private final HashMap<UUID, ActorRef> devicesRefs = new HashMap<>();
    private       boolean                 isMain      = false;
    
    public DeviceManager(ActorRef supervisor) {
        super(supervisor);
    }
    
    public static Props props(ActorRef supervisor) {
        return Props.create(DeviceManager.class, supervisor);
    }
    
    @Override
    public void preStart() {
        log.info("DeviceManager started");
    }
    
    @Override
    public void postStop() {
        log.info("DeviceManager stopped");
    }
    
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ListDevices.class, this::listDevices)
                .match(GetDevice.class, this::getDevice)
                .match(AddDevice.class, this::addDevice)
                .match(RegisterDevice.class, this::registerDevice)
                .match(EditDevice.class, this::editDevice)
                .match(EditDeviceWithID.class, this::editDeviceWithID)
                .match(DeleteDevice.class, this::deleteDevice)
                .match(IoTSupervisor.MainChange.class, this::mainChange)
                .build();
    }
    
    
    public void listDevices(ListDevices listDevices) throws InterruptedException, TimeoutException {
        List<Future<Object>> futureDevices = new ArrayList<>();
        List<Device.DeviceData> devices = new ArrayList<>();
        
        for (ActorRef deviceRef : devicesRefs.values()) {
            futureDevices.add(Patterns.ask(deviceRef, new Device.DeviceInfo(), DEFAULT_AKKA_ASK_TIMEOUT * 1000));
        }
        
        for (Awaitable<Object> futureRoom : futureDevices) {
            devices.add((Device.DeviceData) Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS)));
        }
        sender().tell(new DeviceList(devices), self());
    }
    
    public void getDevice(GetDevice deviceInfo) {
        ActorRef deviceRef = devicesRefs.get(deviceInfo.id);
        if (deviceRef == null) {
            sender().tell(new Messages.NotFound("Device not found"), self());
            return;
        }
        
        deviceRef.tell(new Device.DeviceInfo(), sender());
    }
    
    public void addDevice(AddDevice deviceInfo) throws InterruptedException, TimeoutException, DeviceActorFactory.InvalidDeviceException {
        Object roomRef = null;
        
        if (deviceInfo.roomRefId != null) {
            Future<Object> futureRoom = Patterns.ask(supervisor, new RoomManager.GetRoomRef(deviceInfo.roomRefId), DEFAULT_AKKA_ASK_TIMEOUT * 1000);
            roomRef = Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
            // if roomRef is not an instanceof ActorRef, it is an error
            if (roomRef instanceof Messages) {
                // error detected by the actor
                sender().tell(roomRef, self());
                return;
            }
            if (!(roomRef instanceof ActorRef)) {
                sender().tell(new Messages.Error("Room not found"), self());
                return;
            }
        }
        
        ActorRef deviceRef = getContext().actorOf(DeviceActorFactory.props(deviceInfo.deviceType, deviceInfo.id, (ActorRef) roomRef));
        devicesRefs.put(deviceInfo.id, deviceRef);
        
        deviceRef.tell(new Device.DeviceInfo(), sender());
    }
    
    public void registerDevice(RegisterDevice message) throws InterruptedException, TimeoutException,
            DeviceActorFactory.InvalidDeviceException {
        Object roomRef = null;
        Device.DeviceData deviceInfo = message.deviceInfo;
        
        if (deviceInfo.room != null) {
            Future<Object> futureRoom = Patterns.ask(supervisor, new RoomManager.GetRoomRef(deviceInfo.room.id),
                    DEFAULT_AKKA_ASK_TIMEOUT * 1000);
            roomRef = Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
            // if roomRef is not an instanceof ActorRef, it is an error
            if (roomRef instanceof Messages) {
                // error detected by the actor
                sender().tell(roomRef, self());
                return;
            }
            if (!(roomRef instanceof ActorRef)) {
                sender().tell(new Messages.Error("Room not found"), self());
                return;
            }
        }
        
        ActorRef deviceRef = message.ref;
        devicesRefs.put(deviceInfo.id, deviceRef);
    }
    
    public void editDevice(EditDevice deviceInfo) throws InterruptedException, TimeoutException {
        ActorRef deviceRef = devicesRefs.get(deviceInfo.id);
        Object roomRef = null;
        
        if (deviceRef == null) {
            sender().tell(new Messages.NotFound("Device not found"), self());
            return;
        }
        
        if (deviceInfo.roomRefId != null) {
            Future<Object> futureRoom = Patterns.ask(supervisor, new RoomManager.GetRoomRef(deviceInfo.roomRefId), DEFAULT_AKKA_ASK_TIMEOUT * 1000);
            roomRef = Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
            // if roomRef is not an instanceof ActorRef, it is an error
            if (roomRef instanceof Messages) {
                // error detected by the actor
                sender().tell(roomRef, self());
                return;
            }
            if (!(roomRef instanceof ActorRef)) {
                sender().tell(new Messages.Error("Room not found"), self());
                return;
            }
        }
        
        log.info("Edited device with id: " + deviceInfo.id);
        deviceRef.tell(new Device.EditDevice((ActorRef) roomRef), sender());
    }
    
    public void editDeviceWithID(EditDeviceWithID deviceInfo) throws InterruptedException, TimeoutException {
        EditDevice editDeviceNew = new EditDevice(deviceInfo.deviceType, deviceInfo.roomRefId);
        editDeviceNew.setId(deviceInfo.id);
        editDevice(editDeviceNew);
    }
    
    public void deleteDevice(DeleteDevice deleteDevice) throws InterruptedException {
        ActorRef deviceRef = devicesRefs.get(deleteDevice.id);
        if (deviceRef == null) {
            sender().tell(new Messages.NotFound("Device not found"), self());
            return;
        }
        
        // reading device before deleting
        Future<Object> futureDevice = Patterns.ask(deviceRef, new Device.DeviceInfo(), DEFAULT_AKKA_ASK_TIMEOUT * 1000);
        Object device = null;
        try {
            device = Await.result(futureDevice, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    
        log.info("Removed device with id: " + deleteDevice.id);
        getContext().stop(deviceRef);
        devicesRefs.remove(deleteDevice.id);
        sender().tell(device, self());
    }
    
    private void mainChange(IoTSupervisor.MainChange roomManager) {
        isMain = roomManager.isMain;
    }
    
    
    public static class DeviceManagerMessage extends IoTSerializable {
    
    }
    
    public static class ListDevices extends DeviceManagerMessage {
    
    }
    
    public static class DeviceList extends DeviceManagerMessage {
        
        public final Device.DeviceData[] devices;
        
        public DeviceList(List<Device.DeviceData> devices) {
            this.devices = devices.toArray(new Device.DeviceData[0]);
        }
        
        public DeviceList(@JsonProperty("devices") Device.DeviceData[] devices) {
            this.devices = devices;
        }
        
    }
    
    public static class GetDevice extends DeviceManagerMessage {
        
        private final UUID id;
        
        public GetDevice(UUID id) {
            this.id = id;
        }
        
    }
    
    public static class RegisterDevice extends DeviceManagerMessage {
        
        public final ActorRef          ref;
        public final Device.DeviceData deviceInfo;
        
        public RegisterDevice(@JsonProperty("ref") ActorRef ref, @JsonProperty("deviceData") Device.DeviceData deviceInfo) {
            this.ref = ref;
            this.deviceInfo = deviceInfo;
        }
        
    }
    
    public static class AddDevice extends DeviceManagerMessage {
        
        public final String deviceType;
        public final UUID   id;
        public final UUID   roomRefId;
        
        public AddDevice(@JsonProperty("deviceType") String deviceType, @JsonProperty("room") UUID roomRefId) {
            id = UUID.randomUUID();
            this.deviceType = deviceType;
            this.roomRefId = roomRefId;
        }
        
    }
    
    public static class EditDevice extends DeviceManagerMessage {
        
        public final String deviceType;
        public final UUID   roomRefId;
        public       UUID   id;
        
        public EditDevice(@JsonProperty("deviceType") String deviceType, @JsonProperty("room") UUID roomRefId) {
            this.deviceType = deviceType;
            this.roomRefId = roomRefId;
        }
        
        public void setId(UUID id) {
            this.id = id;
        }
        
    }
    
    public static class EditDeviceWithID extends DeviceManagerMessage {
        
        public final String deviceType;
        public final UUID   roomRefId;
        public final UUID   id;
        
        public EditDeviceWithID(@JsonProperty("id") UUID id, @JsonProperty("deviceType") String deviceType, @JsonProperty("room") UUID roomRefId) {
            this.id = id;
            this.deviceType = deviceType;
            this.roomRefId = roomRefId;
        }
        
    }
    
    public static class DeleteDevice extends DeviceManagerMessage {
        
        private final UUID id;
        
        public DeleteDevice(UUID id) {
            this.id = id;
        }
        
    }
    
}
