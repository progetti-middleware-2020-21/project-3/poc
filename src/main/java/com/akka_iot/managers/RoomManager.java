package com.akka_iot.managers;

import akka.pattern.Patterns;
import com.akka_iot.IoTSupervisor;
import com.akka_iot.Room;
import com.akka_iot.messages.IoTSerializable;
import com.akka_iot.messages.Messages;
import com.fasterxml.jackson.annotation.JsonCreator;
import scala.concurrent.Await;
import scala.concurrent.Awaitable;
import scala.concurrent.Future;
import akka.actor.Props;
import akka.actor.ActorRef;
import com.fasterxml.jackson.annotation.JsonProperty;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.lang.Float.NaN;

/**
 * Manages the rooms in a house
 */
public class RoomManager extends AbstractManager {
    
    private boolean isMain = false;
    
    private final HashMap<UUID, ActorRef> roomsRefs = new HashMap<>();

    public RoomManager(ActorRef supervisor) {
        super(supervisor);
    }

    public static Props props(ActorRef supervisor) {
        return Props.create(RoomManager.class, supervisor);
    }

    @Override
    public void preStart() {
        log.info("RoomManager started");
    }

    @Override
    public void postStop() {
        log.info("RoomManager stopped");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AddRoom.class, this::addRoom)
                .match(DeleteRoom.class, this::deleteRoom)
                .match(ListRooms.class, this::listRooms)
                .match(GetRoom.class, this::getRoom)
                .match(GetRoomRef.class, this::getRoomRef)
                .match(RoomIntel.class, this::roomIntel)
                .match(Room.RoomMessage.class, this::roomMessageForward)
                .match(IoTSupervisor.MainChange.class, this::mainChange)
                .match(IoTSupervisor.NewSupervisor.class, this::newSupervisor)
                .build();
    }
    
    private void roomMessageForward(Room.RoomMessage roomMessage) {
        ActorRef room = roomsRefs.get(roomMessage.id);
        if (room != null) {
            room.tell(roomMessage, getSender());
        }
    }
    
    private void roomIntel(RoomIntel roomIntel) {
        if (!roomsRefs.containsKey(roomIntel.id)) {
            ActorRef roomRef = createRoom(roomIntel.name, roomIntel.floor, roomIntel.size, roomIntel.id, roomIntel.temperature);
            roomRef.tell(roomIntel, getSelf());
        }
    }
    
    private void newSupervisor(IoTSupervisor.NewSupervisor newSupervisor) {
        roomsRefs.values().forEach((roomActor) -> {
            roomActor.forward(newSupervisor, getContext());
        });
    }
    
    private void mainChange(IoTSupervisor.MainChange roomManager) {
        isMain = roomManager.isMain;
    }
    
    
    public void addRoom(AddRoom addRoom) {
        UUID id = UUID.randomUUID();
        createRoom(addRoom.name, addRoom.floor, addRoom.size, id, NaN);
    }
    
    private ActorRef createRoom(String name, int floor, int size, UUID id, float temperature) {
        ActorRef roomRef = getContext().actorOf(Room.props(name, floor, size, id, temperature), "room-"+ id);
        roomsRefs.put(id, roomRef);
        
        log.info("Added room with id: " + id);
        
        roomRef.tell(new Room.RoomInfo(), sender());
        return roomRef;
    }
    
    public static class RoomManagerMessage extends IoTSerializable {}

    public static class AddRoom extends RoomManagerMessage {
        public final String name;
        public final int floor;
        public final int size;

        public AddRoom(@JsonProperty("name") String name, @JsonProperty("floor") int floor, @JsonProperty("size") int size){
            this.name = name;
            this.floor = floor;
            this.size = size;
        }
    }

    public void deleteRoom(DeleteRoom deleteRoom) throws InterruptedException, TimeoutException {
        ActorRef roomRef = roomsRefs.get(deleteRoom.id);
        if (roomRef == null){
            sender().tell(new Messages.NotFound("Room not found"), self());
            return;
        }

        // reading room before deleting
        Future<Object> futureRoom = Patterns.ask(roomRef, new Room.RoomInfo(), DEFAULT_AKKA_ASK_TIMEOUT*1000);
        Object room = Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));

        log.info("Removed room with id: " + deleteRoom.id);
        getContext().stop(roomRef);
        roomsRefs.remove(deleteRoom.id);
        sender().tell(room, self());
    }

    public static class DeleteRoom extends RoomManagerMessage  {
        private final UUID id;
        public DeleteRoom(@JsonProperty("id") UUID id){
            this.id = id;
        }
    }

    public void listRooms(ListRooms listRooms) throws InterruptedException, TimeoutException {
        List<Future<Object>> futureRooms = new ArrayList<>();
        List<Room.RoomData> rooms = new ArrayList<>();

        for (ActorRef room: roomsRefs.values()){
            futureRooms.add(Patterns.ask(room, new Room.RoomInfo(), DEFAULT_AKKA_ASK_TIMEOUT * 1000));
        }

        for (Awaitable<Object> futureRoom: futureRooms){
            rooms.add((Room.RoomData) Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS)));
        }
        sender().tell(new RoomList(rooms), self());
    }
    
    public static class RoomList extends DeviceManager.DeviceManagerMessage {
        public final Room.RoomData[] rooms;
        
        public RoomList(List<Room.RoomData> rooms) {
            this.rooms = rooms.toArray(new Room.RoomData[0]);
        }
        
        public RoomList(@JsonProperty("rooms") Room.RoomData[] rooms) {
            this.rooms = rooms;
        }
        
    }

    public static class ListRooms extends RoomManagerMessage  {}

    public void getRoom(GetRoom roomInfo) throws InterruptedException, TimeoutException {
        ActorRef roomRef = roomsRefs.get(roomInfo.id);
        if (roomRef == null){
            sender().tell(new Messages.NotFound("Room not found"), self());
            return;
        }

        roomRef.tell(new Room.RoomInfo(), sender());
    }

    public static class GetRoom extends RoomManagerMessage {
        private final UUID id;
        public GetRoom(UUID id){
            this.id = id;
        }
    }

    public void getRoomRef(GetRoomRef roomInfo) {
        ActorRef roomRef = roomsRefs.get(roomInfo.id);
        if (roomRef == null){
            sender().tell(new Messages.NotFound("Room not found"), self());
            return;
        }

        sender().tell(roomRef, self());
    }

    public static class GetRoomRef extends RoomManagerMessage {
        private final UUID id;
        public GetRoomRef(UUID id){
            this.id = id;
        }
    }
    
    public static class RoomIntel extends RoomManagerMessage {
        public final String name;
        public final int floor;
        public final int size;
        public final UUID id;
        public final float temperature;
        public final float targetTemperature;
        public final String[] sensors;
        
        @JsonCreator
        public RoomIntel(
                @JsonProperty("name") String name,
                @JsonProperty("floor") int floor,
                @JsonProperty("size") int size,
                @JsonProperty("id") UUID id, @JsonProperty("sensor") String[] sensors,
                @JsonProperty("temperature") float temperature,
                @JsonProperty("targetTemperature") float targetTemperature
        ){
            this.name = name;
            this.floor = floor;
            this.size = size;
            this.id = id;
            this.temperature = temperature;
            this.sensors = sensors;
            this.targetTemperature = targetTemperature;
        }
    }
}
