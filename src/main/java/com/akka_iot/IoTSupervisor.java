package com.akka_iot;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.server.Route;
import com.akka_iot.devices.Device;
import com.akka_iot.managers.DeviceManager;
import com.akka_iot.managers.RoomManager;
import com.akka_iot.messages.IoTSerializable;

import java.util.*;
import java.util.concurrent.CompletionStage;

/**
 * First actor called by the IoTMain (where the ActorSystem is started) IotSupervisor will start the managers: * RoomManager * DeviceManager and run the akka-http server to enable
 * the user interaction with the REST interface
 */
public class IoTSupervisor extends AbstractActor {
    
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    
    private ActorRef          deviceManager;
    private ActorRef          roomManager;
    private Member            supervisor;
    private List<Member>      supervisors = new ArrayList<>();
    private boolean           isMain      = false;
    private Map<String, UUID> devices     = new HashMap<>();
    
    private CompletionStage<ServerBinding> httpServerBinding = null;
    
    public static Props props() {
        return Props.create(IoTSupervisor.class);
    }
    
    @Override
    public void preStart() {
        log.info("iot Application started");
        deviceManager = getContext().actorOf(DeviceManager.props(self()), "device-manager");
        roomManager = getContext().actorOf(RoomManager.props(self()), "room-manager");
        
        Cluster cluster = Cluster.get(getContext().getSystem());
        cluster.subscribe(
                getSelf(),
                ClusterEvent.initialStateAsEvents(),
                ClusterEvent.MemberEvent.class,
                ClusterEvent.UnreachableMember.class);
        
        Route routes = new IotSupervisorHttpRouter(this).getRouter();
        httpServerBinding = Http.get(getContext().getSystem())
                .newServerAt("0.0.0.0", 8080)
                .bind(routes);
    }
    
    @Override
    public void postStop() {
        log.info("Iot Application stopped");
        httpServerBinding.thenCompose(ServerBinding::unbind);
    }
    
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RoomManager.RoomManagerMessage.class, this::forwardRoomManager)
                .match(DeviceManager.DeviceManagerMessage.class, this::forwardDeviceManager)
                .match(Device.DeviceData.class, this::newDevice)
                .match(ClusterEvent.MemberUp.class, this::memberUp)
                .match(ClusterEvent.MemberDowned.class, this::memberDown)
                .build();
    }
    
    public void forwardRoomManager(RoomManager.RoomManagerMessage roomManagerMessage) {
        roomManager.forward(roomManagerMessage, getContext());
        Cluster cluster = Cluster.get(getContext().getSystem());
        if (isMain && ((roomManagerMessage instanceof RoomManager.AddRoom) || (roomManagerMessage instanceof RoomManager.DeleteRoom))) {
            for (Member member : supervisors) {
                if (member != cluster.selfMember()) {
                    getContext().actorSelection(member.address() + "/user/iot-supervisor").forward(roomManagerMessage, getContext());
                }
            }
        }
    }
    
    public void forwardDeviceManager(DeviceManager.DeviceManagerMessage deviceManagerMessage) {
        deviceManager.forward(deviceManagerMessage, getContext());
    }
    
    private void newDevice(Device.DeviceData deviceInfo) {
        log.info("Device info for {}: room {}, type {}", deviceInfo.id, deviceInfo.room,
                deviceInfo.deviceType);
        devices.put(deviceInfo.address.replaceAll("/user/device$", ""), deviceInfo.id);
        deviceManager.tell(new DeviceManager.RegisterDevice(getSender(), deviceInfo), getSelf());
    }
    
    private void memberUp(ClusterEvent.MemberUp mUp) {
        log.info("Member up: {}, {}", mUp.member(), mUp.member().getRoles());
        if (mUp.member().hasRole("device")) {
            getContext().actorSelection(mUp.member().address() + "/user/device").tell(new Device.DeviceInfo(),
                    getSelf());
            log.info("Requested device info");
        } else if (mUp.member().hasRole("supervisor")) {
            supervisors.add(mUp.member());
            supervisor = supervisors.stream().reduce((memberA, memberB) -> {
                if (memberA.hashCode() < memberB.hashCode()) {
                    return memberA;
                } else {
                    return memberB;
                }
            }).orElse(null);
            if (mUp.member() != Cluster.get(getContext().getSystem()).selfMember()) {
                roomManager.tell(new NewSupervisor(mUp.member().address() + "/user/iot-supervisor"), getSelf());
            }
            log.info("Main supervisor: {}", supervisor == null ? "null" : supervisor.address());
            updateMain();
        }
    }
    
    public ActorSelection getMasterSupervisor() {
        return getContext().actorSelection(supervisor.address() + "/user/iot-supervisor");
    }
    
    private void memberDown(ClusterEvent.MemberDowned mDown) {
        if (mDown.member().hasRole("device")) {
            UUID id = devices.remove(mDown.member().address().toString());
            if (id != null) {
                deviceManager.tell(new DeviceManager.DeleteDevice(id), getSelf());
            }
            log.info("Member is down: {}", mDown.member().address());
        } else if (mDown.member().hasRole("supervisor") && (supervisors.contains(mDown.member()))) {
            supervisors.remove(mDown.member());
            supervisor = supervisors.stream().reduce((memberA, memberB) -> {
                if (memberA.hashCode() < memberB.hashCode()) {
                    return memberA;
                } else {
                    return memberB;
                }
            }).orElse(null);
            updateMain();
        }
    }
    
    private void updateMain() {
        Cluster cluster = Cluster.get(getContext().getSystem());
        boolean newIsMain = supervisor == cluster.selfMember();
        if (newIsMain != isMain) {
            isMain = newIsMain;
            log.info(isMain ? "After supervisors change, this supervisor is now main supervisor" : "After supervisors change, this supervisor is no longer main supervisor");
            roomManager.tell(new MainChange(isMain), getSelf());
            deviceManager.tell(new MainChange(isMain), getSelf());
        }
    }
    
    public static class Ping extends IoTSerializable {
    
    }
    
    public static class MainChange {
        
        public final boolean isMain;
        
        public MainChange(boolean isMain) {
            this.isMain = isMain;
        }
        
    }
    
    public static class NewSupervisor extends IoTSerializable {
        
        public final String address;
        
        public NewSupervisor(String address) {
            this.address = address;
        }
        
    }
    
}
