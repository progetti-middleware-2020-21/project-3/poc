package com.akka_iot.devices;

import akka.actor.ActorRef;
import akka.actor.Props;
import com.akka_iot.IoTSupervisor;
import com.akka_iot.messages.IoTSerializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Heat extends Device {
    
    public Heat(UUID id){
        super(id, null);
    }
    
    public boolean active = false;
    
    
    public static Props props(UUID id) {
        return Props.create(Heat.class, id);
    }
    
    public Heat(UUID id, ActorRef roomRef){
        super(id, roomRef);
    }

    public static class ChangeState extends IoTSerializable {
        public final boolean on;
    
        public ChangeState(@JsonProperty("on")boolean on) {
            this.on = on;
        }
    
    }
    
    @Override
    public Receive createReceive() {
        return super.createReceive()
                .orElse(
                        receiveBuilder()
                                .match(ChangeState.class, this::changeState)
                                .build()
                );
    }
    
    private void changeState(ChangeState state) {
        if (active != state.on) {
            this.active = state.on;
            log.info("The heater was {}, now it's {}", this.active ? "off" : "on", this.active ? "on" : "off");
        } else {
            log.info("The heater was already {}, nothing changed", this.active ? "on" : "off");
        }
    }
    
    @Override
    public String getDeviceType() {
        return DeviceActorFactory.HEATING;
    }
}
