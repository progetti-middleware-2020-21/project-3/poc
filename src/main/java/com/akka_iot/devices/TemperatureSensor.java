package com.akka_iot.devices;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.akka_iot.IoTSupervisor;
import com.akka_iot.messages.IoTSerializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class TemperatureSensor extends Device {
    
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    
    public TemperatureSensor(UUID id, ActorRef roomRef) {
        super(id, roomRef);
    }
    
    public TemperatureSensor(UUID id) {
        super(id, null);
    }
    
    
    
    public static final class ReadTemperature extends IoTSerializable {
    }
    
    public static final class RespondTemperature extends IoTSerializable {
        public float value;
        
        public RespondTemperature(@JsonProperty("value") float value){
            this.value = value;
        }
    }
    
    
    
    public static Props props(UUID id) {
        return Props.create(TemperatureSensor.class, id);
    }
    
    @Override
    public void preStart() throws Exception {
        super.preStart();
        log.info("Sensor started! Address: {}", getSelf());
    }
    
    @Override
    public String getDeviceType() {
        return DeviceActorFactory.TEMPERATURE_SENSOR;
    }
    
    @Override
    public Receive createReceive() {
        return super.createReceive()
                .orElse(
                        receiveBuilder()
                                .match(IoTSupervisor.Ping.class, (IoTSupervisor.Ping ping) -> log.info("We have a Ping!"))
                                .match(ReadTemperature.class, this::readTemperature)
                                .build()
                );
    }
    
    private float time = 0;
    
    private void readTemperature(ReadTemperature readTemperature) {
        getSender().tell(new RespondTemperature((float) Math.sin(time) * 4.5f + 20), getSelf());
        time += 0.1;
    }
    
}
