package com.akka_iot.devices;

import akka.actor.ActorRef;
import akka.actor.Props;

import java.util.UUID;

public class DeviceActorFactory {
    public final static String AC = "AC";
    public final static String HEATING = "HEATING";
    public final static String TEMPERATURE_SENSOR = "TEMPERATURE_SENSOR";

    public static Props props(String deviceClassName, UUID id, ActorRef roomRef) throws InvalidDeviceException {
        switch (deviceClassName){
            case AC:
                return DeviceActorFactory.props(AirConditioner.class, id, roomRef);
            case TEMPERATURE_SENSOR:
                return DeviceActorFactory.props(TemperatureSensor.class, id, roomRef);
            case HEATING:
                return DeviceActorFactory.props(Heat.class, id, roomRef);
            default:
                throw new InvalidDeviceException();
        }
    }

    private static Props props(Class<? extends Device> deviceClass, UUID id, ActorRef roomRef) {
        return Props.create(deviceClass, id, roomRef);
    }

    public static class InvalidDeviceException extends Exception{}
}
