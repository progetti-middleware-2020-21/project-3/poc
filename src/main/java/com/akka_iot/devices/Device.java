package com.akka_iot.devices;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.typed.receptionist.ServiceKey;
import akka.cluster.Cluster;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.pattern.Patterns;
import com.akka_iot.Room;
import com.akka_iot.messages.IoTSerializable;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class Device extends AbstractActor {
    
    protected final       LoggingAdapter log                      = Logging.getLogger(getContext().getSystem(), this);
    
    public static final int            DEFAULT_AKKA_ASK_TIMEOUT = 1;
    
    protected final UUID id;
    protected ActorRef roomRef;

    public Device(UUID id, ActorRef roomRef) {
        this.id = id;
        this.roomRef = roomRef;
        if (roomRef != null) {
            Room.RoomData room = null;
            Future<Object> futureRoom = Patterns.ask(roomRef, new Room.RoomInfo(), DEFAULT_AKKA_ASK_TIMEOUT * 1000);
            try {
                room = (Room.RoomData) Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
            } catch (InterruptedException | TimeoutException ignored) {
            }
            this.roomRef.tell(new DeviceData(getDeviceType(), id, room, getSelf().path().toStringWithAddress(Cluster.get(getContext().getSystem()).selfAddress())), getSender());
        }
    }

    public ActorRef getRoomRef() {
        return roomRef;
    }

    public void setRoomRef(ActorRef roomRef) {
        this.roomRef = roomRef;
        
    }

    public UUID getId() {
        return id;
    }

    abstract public String getDeviceType();

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(DeviceInfo.class, this::deviceInfo)
                .match(EditDevice.class, this::editDevice)
                .build();
    }


    public void deviceInfo(DeviceInfo deviceInfo) {
        log.info("Device info requested!");
        if(roomRef == null){
            sender().tell(new DeviceData(getDeviceType(), id, null, getSelf().path().toStringWithAddress(Cluster.get(getContext().getSystem()).selfAddress())), self());
            return;
        }

        Future<Object> futureRoom = Patterns.ask(roomRef, new Room.RoomInfo(), DEFAULT_AKKA_ASK_TIMEOUT*1000);
        Room.RoomData room = null;
        try {
            room = (Room.RoomData) Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
        } catch (InterruptedException | TimeoutException e) {
            this.roomRef = null;
        }

        sender().tell(new DeviceData(getDeviceType(), id, room, getSelf().path().toStringWithAddress(Cluster.get(getContext().getSystem()).selfAddress())), self());
    }

    public static class DeviceInfo extends IoTSerializable {
    }

    public static class DeviceData extends IoTSerializable {
        public final String deviceType;
        public final UUID id;
        public final Room.RoomData room;
        public final String address;

        @JsonCreator
        public DeviceData(
                @JsonProperty("deviceType") String deviceType,
                @JsonProperty("id") UUID id,
                @JsonProperty("room") Room.RoomData room,
                @JsonProperty("address") String address
        ){
            this.deviceType = deviceType;
            this.id = id;
            this.room = room;
            this.address = address;
        }
    }

    public void editDevice(EditDevice deviceInfo) {
        if (this.roomRef == null || !this.roomRef.equals(deviceInfo.roomRef)) {
            ActorRef oldRef = this.roomRef;
            this.roomRef = deviceInfo.roomRef;
            Room.RoomData room = null;
            if (roomRef != null) {
                Future<Object> futureRoom = Patterns.ask(roomRef, new Room.RoomInfo(), DEFAULT_AKKA_ASK_TIMEOUT * 1000);
                try {
                    room = (Room.RoomData) Await.result(futureRoom, Duration.create(DEFAULT_AKKA_ASK_TIMEOUT, TimeUnit.SECONDS));
                } catch (InterruptedException | TimeoutException ignored) {
                }
            }
            if (oldRef != null) {
                oldRef.tell(new DeviceData(getDeviceType(), id, room, getSelf().path().toStringWithAddress(Cluster.get(getContext().getSystem()).selfAddress())), getSender());
            }
            if (roomRef != null) {
                this.roomRef.tell(new DeviceData(getDeviceType(), id, room, getSelf().path().toStringWithAddress(Cluster.get(getContext().getSystem()).selfAddress())), getSender());
            }
        }
        self().tell(new DeviceInfo(), sender());
    }

    public static class EditDevice extends IoTSerializable {
        public final ActorRef roomRef;

        public EditDevice(@JsonProperty("roomRef") ActorRef roomRef) {
            this.roomRef = roomRef;
        }
    }
}
